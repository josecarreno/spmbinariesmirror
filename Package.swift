// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Firebase",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "FirebaseAnalytics",
            targets: [
                "FirebaseAnalytics",
                "FirebaseCore",
                "FirebaseCoreDiagnostics",
                "FirebaseInstallations",
                "GoogleAppMeasurement",
                "GoogleAppMeasurementIdentitySupport",
                "GoogleDataTransport",
                "GoogleUtilities",
                "PromisesObjC",
                "nanopb",
            ]),
        .library(
            name: "FirebaseCrashlytics",
            targets: [
                "FirebaseAnalytics",
                "FirebaseCrashlytics",
            ]),
        .library(
            name: "FirebaseDynamicLinks",
            targets: [
                "FirebaseAnalytics",
                "FirebaseDynamicLinks",
            ]),
    ],
    dependencies: [],
    targets: [
        .binaryTarget(
            name: "FirebaseAnalytics",
            path: "FirebaseAnalytics/FirebaseAnalytics.xcframework"),
        .binaryTarget(
            name: "FirebaseCore",
            path: "FirebaseAnalytics/FirebaseCore.xcframework"),
        .binaryTarget(
            name: "FirebaseCoreDiagnostics",
            path: "FirebaseAnalytics/FirebaseCoreDiagnostics.xcframework"),
        .binaryTarget(
            name: "FirebaseInstallations",
            path: "FirebaseAnalytics/FirebaseInstallations.xcframework"),
        .binaryTarget(
            name: "GoogleAppMeasurement",
            path: "FirebaseAnalytics/GoogleAppMeasurement.xcframework"),
        .binaryTarget(
            name: "GoogleAppMeasurementIdentitySupport",
            path: "FirebaseAnalytics/GoogleAppMeasurementIdentitySupport.xcframework"),
        .binaryTarget(
            name: "GoogleDataTransport",
            path: "FirebaseAnalytics/GoogleDataTransport.xcframework"),
        .binaryTarget(
            name: "GoogleUtilities",
            path: "FirebaseAnalytics/GoogleUtilities.xcframework"),
        .binaryTarget(
            name: "PromisesObjC",
            path: "FirebaseAnalytics/PromisesObjC.xcframework"),
        .binaryTarget(
            name: "nanopb",
            path: "FirebaseAnalytics/nanopb.xcframework"),
        .binaryTarget(
            name: "FirebaseCrashlytics",
            path: "FirebaseCrashlytics/FirebaseCrashlytics.xcframework"),
        .binaryTarget(
            name: "FirebaseDynamicLinks",
            path: "FirebaseDynamicLinks/FirebaseDynamicLinks.xcframework")
        
    ]
)
