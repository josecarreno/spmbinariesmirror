import XCTest

import spmbinariesmirrorTests

var tests = [XCTestCaseEntry]()
tests += spmbinariesmirrorTests.allTests()
XCTMain(tests)
